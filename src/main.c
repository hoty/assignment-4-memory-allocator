#include "mem.h"
#include "tests.h"
#include <stdio.h>

int main()
{
    if (test_usuall_alloc()) return 1;
    if (test_grow_heap()) return 1;
    return test_free();
}
