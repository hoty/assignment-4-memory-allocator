#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

#include <unistd.h>

#define HEAPINITSIZE 100000

#define FIRSTBLOCKSIZE 1000
#define SECONDBLOCKSIZE 5000
#define THIRDBLOCKSIZE 3000

#define VERYBIGMEMALLOC (HEAPINITSIZE * 10)

extern struct block_header* block_get_header(void* contents);
extern size_t region_actual_size( size_t query );

void real_free_mem(void* addr, size_t size) {
    munmap(addr, size);
}

int test_usuall_alloc() {
    printf(" --- Тест: test_usuall_alloc\n");
    struct block_header* heap = (struct block_header*)heap_init(HEAPINITSIZE);
    if (!heap) {
        fprintf(stderr, "Не удалось выделить память\n");
        return 1;
    }
    if (size_from_capacity(heap->capacity).bytes != region_actual_size(HEAPINITSIZE)) {
        fprintf(stderr, "Неверный размер блока памяти\n");
        return 1;
    }
    // простое выделение 1
    struct block_header* ptr1 = block_get_header(_malloc(FIRSTBLOCKSIZE));
    if (!ptr1) {
        fprintf(stderr, "Не удалось выделить первый блок\n");
        return 1;
    }
    if (ptr1->capacity.bytes != FIRSTBLOCKSIZE) {
        fprintf(stderr, "Размер первого блока не тот\n");
        return 1;
    }
    // выделение 2 блока
    struct block_header* ptr2 = block_get_header(_malloc(SECONDBLOCKSIZE));
    if (!ptr2) {
        fprintf(stderr, "Не удалось выделить первый блок\n");
        return 1;
    }
    if (ptr2->capacity.bytes != SECONDBLOCKSIZE) {
        fprintf(stderr, "Размер второго блока не тот\n");
        return 1;
    }
    if (ptr1->next != ptr2) {
        fprintf(stderr, "Второй блок не следует за первым\n");
        return 1;
    }
    // выделение 3 блока
    struct block_header* ptr3 = block_get_header(_malloc(THIRDBLOCKSIZE));
    if (!ptr3) {
        fprintf(stderr, "Не удалось выделить первый блок\n");
        return 1;
    }
    if (ptr3->capacity.bytes != THIRDBLOCKSIZE) {
        fprintf(stderr, "Размер третьего блока не тот\n");
        return 1;
    }
    if (ptr2->next != ptr3) {
        fprintf(stderr, "Третий блок не следует за вторым\n");
        return 1;
    }
    real_free_mem(heap, region_actual_size(HEAPINITSIZE));
    printf(" --- Тест: test_usuall_alloc OK\n");
    return 0;
}

int test_grow_heap() {
    printf(" --- Тест: test_grow_heap\n");
    struct block_header* heap = (struct block_header*)heap_init(HEAPINITSIZE);
    if (!heap) {
        fprintf(stderr, "Не удалось выделить память\n");
        return 1;
    }
    size_t total_size = region_actual_size(HEAPINITSIZE);
    struct block_header* ptr1 = block_get_header(_malloc(FIRSTBLOCKSIZE));
    struct block_header* ptr2 = block_get_header(_malloc(VERYBIGMEMALLOC));
    total_size += region_actual_size(VERYBIGMEMALLOC);
    if (!ptr2) {
        fprintf(stderr, "Расширение кучи не вернуло указатель\n");
        return 1;
    }
    if (ptr1->next != ptr2) {
        fprintf(stderr, "Расширение кучи неверно, второй блок не следует за первым\n");
        return 1;
    }
    if (!ptr2->next) {
        fprintf(stderr, "Расширение кучи неверно, блок не был разделен\n");
        return 1;
    }
    struct block_header* ptr3 = ptr2->next;
    if (size_from_capacity(ptr3->capacity).bytes != (total_size - size_from_capacity(ptr1->capacity).bytes -
                                                     size_from_capacity(ptr2->capacity).bytes)) {
        fprintf(stderr, "Расширение кучи неверно, размер последнего блока неверен\n");
        return 1;
    }
    real_free_mem(heap, total_size);
    printf(" --- Тест: test_grow_heap OK\n");
    return 0;
}

int test_free() {
    printf(" --- Тест: test_free\n");
    struct block_header* heap = (struct block_header*)heap_init(HEAPINITSIZE);
    if (!heap) {
        fprintf(stderr, "Не удалось выделить память\n");
        return 1;
    }
    size_t total_size = region_actual_size(HEAPINITSIZE);
    void* ptr1 = _malloc(FIRSTBLOCKSIZE);
    void* ptr2 = _malloc(SECONDBLOCKSIZE);
    void* ptr3 = _malloc(THIRDBLOCKSIZE);
    _free(ptr3);
    if (!block_get_header(ptr3)->is_free) {
        fprintf(stderr, "Освобождение неверно, блок занят\n");
        return 1;
    }
    _free(ptr2);
    if (block_get_header(ptr2)->next != 0) {
        fprintf(stderr, "Освобождение неверно, блоки не слились\n");
        return 1;
    }
    if (size_from_capacity(block_get_header(ptr2)->capacity).bytes !=
            (total_size - size_from_capacity(block_get_header(ptr1)->capacity).bytes)) {
        fprintf(stderr, "Освобождение неверно, размер объединенного блока не тот\n");
        return 1;
    }
    real_free_mem(heap, total_size);
    printf(" --- Тест: test_free ОК\n");
    return 0;
}
